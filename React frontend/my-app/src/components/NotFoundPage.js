import React, { Component } from 'react';

class NotFoundPage extends Component {
    render() {
        return (
            <h2>404 Not Found</h2>
        )
    }
}

export default NotFoundPage;
