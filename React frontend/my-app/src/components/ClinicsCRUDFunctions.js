import axios from 'axios';

export const getList = () => {
    return axios
        .get('api/hospitals/', {
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => {
            return res.data
        })
}

export const addItem = (unittype, name, location, logo) => {
    return axios
        .post('api/hospitals/',
            {
                unittype: unittype,
                name: name,
                location: location,
                logo: logo
            },
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        .then(res => {
            console.log(res)
        })
}

export const deleteItem = (medicalunitid) => {
    return axios
        .delete(`api/hospitals/${medicalunitid}`, {
            headers: { 'Content-Type': 'application/json' }

        })
        .then(res => {
            console.log(res)
        })
        .catch(err => {
            console.log(err)
        })

}

export const updateItem = (unittype, name, location, logo, medicalunitid) => {
    return axios
        .put(`api/hospitals/${medicalunitid}/`,
            {
                medicalunitid: medicalunitid,
                unittype: unittype,
                name: name,
                location: location,
                logo: logo
            },
            {
                headers: { 'Content-Type': 'application/json' }

            })
        .then(res => {
            console.log(res)
        })
        .catch(err => {
            console.log(err)
        })

}