import React, { Component } from 'react';
import Image from 'react-bootstrap/Image';
import MuresanAnca from '../images/muresan_anca.png';
import NeculaAlexandru from '../images/necula_alexandru.png';
import OsianGelu from '../images/osian_gelu.png';
import axios from 'axios';

const Doctor = (props) => (
  <div>
    <div class="container">
      <Image src={props.profilePicture} className="scale-with-grid" alt="Avatar" class="image" />
      <div class="overlay">
        <div class="text">{props.specialty}</div>
      </div>
    </div>
    <h2>{props.fullName}</h2>
    <p>{props.graduationYear}</p>
  </div>
);
const Doctor1 = (props) => (
  <div class="overlay">
    <div class="text">{props.specialty}</div>
  </div>
);

const Doctor2 = (props) => (
  <div>
    <h2>{props.fullName}</h2>
    <h4>Data absolvirii: </h4><p>{props.graduationYear}</p>

  </div>
);

class HyperclinicaMedlife extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctors: [] //in stare am acest array
    };
  }

  //toate rest callurile se fac aici
  async componentDidMount() {
    const result = await axios.get("http://localhost/cosmina/medlife.php");
    this.setState({
      doctors: result.data //populam state-ul cu ~ v[i] elem curent, din backend
    });
  }

    render() {
      return (
        // <div>
        // <div className="medsan-container">
        //   <h2>Medsan</h2>
        //   <div class="row">
        //     <div class="column" >
        //       <div class="container">
        //         <Image src={CotaCristian} className="scale-with-grid" alt="Avatar" class="image" />
        //         {this.state.doctors.filter(i => i.doctorID == 7).map(i => <Doctor1 {...i} />)}
        //       </div>
        //       {this.state.doctors.filter(i => i.doctorID == 7).map(i => <Doctor2 {...i} />)}
        //     </div>

        //     <div class="column" >
        //       <div class="container">
        //         <Image src={DragotaTudor} className="scale-with-grid" alt="Avatar" class="image" />
        //         {this.state.doctors.filter(i => i.doctorID == 8).map(i => <Doctor1 {...i} />)}
        //       </div>
        //       {this.state.doctors.filter(i => i.doctorID == 8).map(i => <Doctor2 {...i} />)}
        //     </div>

        //     <div class="column" >
        //       <div class="container">
        //         <Image src={AlexStanciu} className="scale-with-grid" alt="Avatar" class="image" />
        //         {this.state.doctors.filter(i => i.doctorID == 9).map(i => <Doctor1 {...i} />)}
        //       </div>
        //       {this.state.doctors.filter(i => i.doctorID == 9).map(i => <Doctor2 {...i} />)}

        //     </div>
        //   </div>
        // </div>
     // <div>
     ///////////////////
        <div className="medsan-container">
          <h2>Hyperclinica Medlife</h2>
          <div class="row">
            <div class="column" >
              <div class="container">
                <Image src={MuresanAnca} className="scale-with-grid" alt="Avatar" class="image" />
                <div class="overlay">
                  <div class="text">medic homeopat</div>
                </div>
              </div>
              DR. MURESAN ANCA
            <p>An absolvire: 2003</p>
            </div>
            <div class="column" >
              <div class="container">
                <Image src={NeculaAlexandru} className="scale-with-grid" alt="Avatar" class="image" />
                <div class="overlay">
                  <div class="text">medic psiholog</div>
                </div>
              </div>
              DR.  NECULA ALEXANDRU
            <p>An absolvire: 2004</p>
            </div>
            <div class="column" ><div class="container">
              <Image src={OsianGelu} className="scale-with-grid" alt="Avatar" class="image" />
              <div class="overlay">
                <div class="text">medic oncolog</div>
              </div>
            </div>
              DR. OSIAN GELU
            <p>An absolvire: 2002</p>
            </div>
          </div>
        </div>
  
      )
    }
  }

  export default HyperclinicaMedlife;