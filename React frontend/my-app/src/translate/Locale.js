import localize from 'fronto-localize';

const en={
  Nume:'Name',
  Tip:'Type',
  Locatie:'Location',
  Sigla:'Logo',
  NumeSite:'Welcome to Cosmina\'s Medical Information Site',
  Acasa:'Home',
  Medici:'Doctors',
  Unitati:'Clinics',
  Info:'Information',
  Slide2:'To keep the body in good health is a duty... otherwise we shall not be able to keep our mind strong and clear.',
  Slide3:'All the money in the world can\'t buy you back good health.',
  Login: 'Login',
  Register:'Register',
  User: 'User page',
  Logout: 'Logout',
  Admin: 'Admin',
  Statistics:'Statistics',
  Map: 'Map'
}

const ro={
  Nume:'Nume',
  Tip:'Tip',
  Locatie:'Locatie',
  Sigla:'Sigla',
  NumeSite:'Bine ati venit la Cosmina\'s Medical Information Site',
  Acasa:'Acasa',
  Medici:'Medici',
  Unitati:'Clinici',
  Info:'Informatii',
  Slide2:'Pentru a menține corpul în stare bună de sănătate este o datorie ... altfel nu vom putea să ne ținem mintea puternică și clară.',
  Slide3:'Toți banii din lume nu vă pot cumpăra sănătate bună.',
  Login: 'Logare',
  Register:'Creare cont',
  User: 'Pagina Utilizator',
  Logout:'Deconectare',
  Admin: 'Admin',
  Statistics:'Statistici',
  Map:'Harta'

}

export default localize({en,ro});