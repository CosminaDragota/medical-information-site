import axios from 'axios';

export const getList = () => {
    return axios
        .get('api/users/', {
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => {
            return res.data
        })
}

export const deleteItem = (medicalunitid) => {
    return axios
        .delete(`api/users/${medicalunitid}`, {
            headers: { 'Content-Type': 'application/json' }

        })
        .then(res => {
            console.log(res)
        })
        .catch(err => {
            console.log(err)
        })

}