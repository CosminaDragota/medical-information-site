from enum import Enum

class SpecialtyChoice(Enum):   # A subclass of Enum
    FAMILIE = "medic de familie"
    PSIHOTERAPEUT = "medic psihoterapeut"
    HOMEOPAT = "medic homeopat"
    ONCOLOG = "medic oncolog"
    GINECOLOG = "medic ginecolog"
    PSIHOLOG = "medic phisolog"