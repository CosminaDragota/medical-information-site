<?php

header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

$con = mysqli_connect("localhost", "root", "", "cosmina");
// Check connection
if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

	$sql="SELECT d.doctorID, d.fullName, d.graduationYear, d.specialty 
            FROM doctors d LEFT JOIN doctorspermedicalunit dm ON d.doctorID=dm.fkDoctors
            LEFT JOIN medicalunits m ON dm.fkMedicalUnits = m.medicalUnitID
            where dm.fkMedicalUnits=1";

	$doctors=[];
	$result = mysqli_query($con,$sql);

	if (!$result) {
		die('Could not query:' . mysql_error());
	}
	if($result=mysqli_query($con, $sql))
	{
	//Fetch one and one row
		while($row=mysqli_fetch_object($result))
		{
			$doctors[]=$row;		
		}
		//Free result set
		mysqli_free_result($result);
	}


mysqli_close($con);

//Send data to front-end
echo json_encode($doctors);
