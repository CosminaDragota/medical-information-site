import React, { Component } from 'react';
import Image from 'react-bootstrap/Image';
import GraurFlorin from '../images/graur_florin.png';
import MocanLucian from '../images/mocan_lucian.png';
import VidaRadu from '../images/IMG_1939.jpg';
import FelecanAda from '../images/IMG_1975.jpg';

import axios from 'axios';

const Doctor = (props) => (
  <div>
    <div class="container">
      <Image src={props.profilePicture} className="scale-with-grid" alt="Avatar" class="image" />
      <div class="overlay">
        <div class="text">{props.specialty}</div>
      </div>
    </div>
    <h2>{props.fullName}</h2>
    <p>{props.graduationYear}</p>
  </div>
);

const Doctor1 = (props) => (
  <div class="overlay">
    <div class="text">{props.specialty}</div>
  </div>
);

const Doctor2 = (props) => (
  <div>
    <h2>{props.fullName}</h2>
    <h4>Data absolvirii: </h4><p>{props.graduationYear}</p>

  </div>
);


class PoliclinicaPromedis extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctors: [] //in stare am acest array
    };
  }

  //toate rest callurile se fac aici
  async componentDidMount() {
    const result = await axios.get("http://localhost/cosmina/promedis.php");
    this.setState({
      doctors: result.data //populam state-ul cu ~ v[i] elem curent, din backend
    });
  } 
  render() {
      return (
        <div>
        <div className="medsan-container">
          <h2>Medsan</h2>
          <div class="row">
            <div class="column" >
              <div class="container">
                <Image src={VidaRadu} className="scale-with-grid" alt="Avatar" class="image" />
                {this.state.doctors.filter(i => i.doctorID == 22).map(i => <Doctor1 {...i} />)}
              </div>
              {this.state.doctors.filter(i => i.doctorID == 22).map(i => <Doctor2 {...i} />)}
            </div>
  
            <div class="column" >
              <div class="container">
                <Image src={FelecanAda} className="scale-with-grid" alt="Avatar" class="image" />
                {this.state.doctors.filter(i => i.doctorID == 23).map(i => <Doctor1 {...i} />)}
              </div>
              {this.state.doctors.filter(i => i.doctorID == 23).map(i => <Doctor2 {...i} />)}
            </div>
  
            <div class="column" >
              <div class="container">
                <Image src={MocanLucian} className="scale-with-grid" alt="Avatar" class="image" />
                {this.state.doctors.filter(i => i.doctorID == 24).map(i => <Doctor1 {...i} />)}
              </div>
              {this.state.doctors.filter(i => i.doctorID == 24).map(i => <Doctor2 {...i} />)}
  
            </div>
          </div>
        </div>
      </div>
      //   <div>
      //   <div className="medsan-container">
      //     <h2>Promedis</h2>
      //     <div class="row">
      //       <div class="column" >
      //         {this.state.doctors.map(i => <Doctor {...i} />)} 
      //         {/*pt fiecare element din state, vom randa un nou doctor cu propr coresp din state */}
      //       </div>
      //     </div>
      //   </div>
      // </div>
      ////////////
      //   <div className="medsan-container">
      //   <h2>Policlinica Promedis</h2>
      //   <div class="row">
      //     <div class="column" >
      //       <div class="container">
      //         <Image src={GraurFlorin} className="scale-with-grid" alt="Avatar" class="image" />
      //         <div class="overlay">
      //           <div class="text">medic de familie</div>
      //         </div>
      //       </div>
      //       DR. FETTI ALIN
      //     <p>An absolvire: 2017</p>
      //     </div>
      //     <div class="column" >
      //       <div class="container">
      //         <Image src={MocanLucian} className="scale-with-grid" alt="Avatar" class="image" />
      //         <div class="overlay">
      //           <div class="text">medic oncolog</div>
      //         </div>
      //       </div>
      //       DR.   BODEA MARIUS
      //     <p>An absolvire: 2004</p>
      //     </div>
      //     <div class="column" ><div class="container">
      //       <Image src={VidaRadu} className="scale-with-grid" alt="Avatar" class="image" />
      //       <div class="overlay">
      //         <div class="text">medic ginecolog</div>
      //       </div>
      //     </div>
      //       DR. HAJAN NADIM
      //     <p>An absolvire: 2016</p>
      //     </div>
      //   </div>
      // </div>
  
      )
    }
  }

  export default PoliclinicaPromedis;