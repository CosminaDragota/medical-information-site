import React, { Component } from 'react';
import Image from 'react-bootstrap/Image';
import IancuCornel from '../images/iancu-cornel.jpg';
import FelecanAda from '../images/IMG_1975.jpg';
import VidaRadu from '../images/IMG_1939.jpg';
import MocanLucian from '../images/mocan_lucian.png';
import MolnarGeza from '../images/molnar_geza.png';
import MunteanuDoru from '../images/munteanu_doru.png';
import axios from 'axios';

const Doctor = (props) => (
  <div>
    <div class="container">
      <Image src={props.profilePicture} className="scale-with-grid" alt="Avatar" class="image" />
      <div class="overlay">
        <div class="text">{props.specialty}</div>
      </div>
    </div>
    <h2>{props.fullName}</h2>
    <p>{props.graduationYear}</p>
  </div>
);

const Doctor1 = (props) => (
  <div class="overlay">
    <div class="text">{props.specialty}</div>
  </div>
);

const Doctor2 = (props) => (
  <div>
    <h2>{props.fullName}</h2>
    <h4>Data absolvirii: </h4><p>{props.graduationYear}</p>

  </div>
);


class PoliclinicaMedstarManastur extends Component { 
  constructor(props) {
    super(props);
    this.state = {
      doctors: [] //in stare am acest array
    };
  }

  //toate rest callurile se fac aici
  async componentDidMount() {
    const result = await axios.get("http://localhost/cosmina/medstar.php");
    this.setState({
      doctors: result.data //populam state-ul cu ~ v[i] elem curent, din backend
    });
  }

  render() {
    return (
      <div>
      <div className="medsan-container">
        <h2>Medsan</h2>
        <div class="row">
          <div class="column" >
            <div class="container">
              <Image src={VidaRadu} className="scale-with-grid" alt="Avatar" class="image" />
              {this.state.doctors.filter(i => i.doctorID == 4).map(i => <Doctor1 {...i} />)}
            </div>
            {this.state.doctors.filter(i => i.doctorID == 4).map(i => <Doctor2 {...i} />)}
          </div>

          <div class="column" >
            <div class="container">
              <Image src={MocanLucian} className="scale-with-grid" alt="Avatar" class="image" />
              {this.state.doctors.filter(i => i.doctorID == 5).map(i => <Doctor1 {...i} />)}
            </div>
            {this.state.doctors.filter(i => i.doctorID == 5).map(i => <Doctor2 {...i} />)}
          </div>

          <div class="column" >
            <div class="container">
              <Image src={MunteanuDoru} className="scale-with-grid" alt="Avatar" class="image" />
              {this.state.doctors.filter(i => i.doctorID == 6).map(i => <Doctor1 {...i} />)}
            </div>
            {this.state.doctors.filter(i => i.doctorID == 6).map(i => <Doctor2 {...i} />)}

          </div>
        </div>
      </div>
    </div>
      // <div>
      //   <div className="medsan-container">
      //     <h2>Medstar</h2>
      //     <div class="row">
      //       <div class="column" >
      //         {this.state.doctors.map(i => <Doctor {...i} />)} 
      //         {/*pt fiecare element din state, vom randa un nou doctor cu propr coresp din state */}
      //       </div>
      //     </div>
      //   </div>
      // </div>
      ///////////////
      // <div className="medsan-container">
      //   <h2>Policlinica Medstar Manastur</h2>
      //   <div class="row">
      //     <div class="column" >
      //       <div class="container">
      //         <Image src={IancuCornel} className="scale-with-grid" alt="Avatar" class="image" />
      //         <div class="overlay">
      //           <div class="text">medic oncolog</div>
      //         </div>
      //       </div>
      //       DR. IANCU CORNEL
      //     <p>An absolvire: 2005</p>
      //     </div>
      //     <div class="column" >
      //       <div class="container">
      //         <Image src={FelecanAda} className="scale-with-grid" alt="Avatar" class="image" />
      //         <div class="overlay">
      //           <div class="text">medicina de familie</div>
      //         </div>
      //       </div>
      //       DR. FELECAN ANA
      //     <p>An absolvire: 2003</p>
      //     </div>
      //     <div class="column" ><div class="container">
      //       <Image src={VidaRadu} className="scale-with-grid" alt="Avatar" class="image" />
      //       <div class="overlay">
      //         <div class="text">medic de familie</div>
      //       </div>
      //     </div>
      //       DR. VIDA RADU
      //     <p>An absolvire: 2001</p>
      //     </div>
      //   </div>
      // </div>

    )
  }
}

export default PoliclinicaMedstarManastur;