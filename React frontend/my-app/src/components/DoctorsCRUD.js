import React, { Component } from 'react';
import { getList, addItem, deleteItem, updateItem } from './DoctorCRUDFunctions';
import { Redirect } from 'react-router-dom';

const imgMyimageexample = require('../images/background/doctor1.jpg');
const divStyle = {
    height: '100%',
    backgroundColor: 'burlywood'
};

class DoctorsCRUD extends Component {
    constructor() {
        super()
        this.state = {
            doctorid: '',
            specialty: '',
            fullname: '',
            graduationyear: '',
            profilePicture: '',
            editDisabled: false,
            items: []
        }
    }

    componentDidMount() {
        this.getAll()
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleNameChange = e => {
        this.setState({
            fullname: e.target.value
        })
    }

    handleSpecialtyChange = e => {
        this.setState({
            specialty: e.target.value
        })
    }

    handleGraduationYearChange = e => {
        this.setState({
            graduationyear: e.target.value
        })
    }

    getAll = () => {
        getList().then(data => {
            this.setState({
                specialty: '',
                fullname: '',
                graduationyear: '',
                profilePicture: '',
                items: [...data]
            },
                () => {
                    console.log(this.state.items)
                })
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        addItem(this.state.fullname, this.state.graduationyear, this.state.specialty, this.state.profilepicture).then(() => {
            this.getAll()
        })
        this.setState({
            fullname: '',
            specialty: '',
            graduationyear: ''
        })
    }

    onUpdate = (e) => {
        e.preventDefault()
        updateItem(
            this.state.fullname, this.state.specialty, this.state.graduationyear, this.state.profilepicture, this.state.doctorid
        ).then(() => {
            this.getAll()
        })
        this.setState({
            fullname: '',
            specialty: '',
            graduationyear: '',
            editDisabled: ''
        })
        this.getAll()
    }

    onEdit = (itemid, e) => {
        e.preventDefault();

        sessionStorage.setItem('doctor', JSON.stringify(this.state));
        console.log("On edit", itemid);

        var data = [...this.state.items]
        data.forEach((item, index) => {
            if (item.doctorid === itemid) {
                console.log("true");
                this.setState({
                    doctorid: item.doctorid,
                    specialty: item.specialty,
                    fullname: item.fullname,
                    graduationyear: item.graduationyear,
                    editDisabled: true
                })
            }
        })
    }

    onDelete = (val, e) => {
        e.preventDefault();
        deleteItem(val);
        this.getAll();
    }

    render() {
        let user = sessionStorage.getItem('userData');
        let objectUser = JSON.parse(user);
        if (user) {

            if (objectUser.userType === "admin") {

                return (
                    <div className="page-container-links" style={divStyle}>
                        <div className="col-md-12">
                            <form onSubmit={this.onSubmit}>
                                <div className='form-group'>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <label htmlFor='fullname'>Full Name</label>
                                            <input type="text"
                                                className='form-control'
                                                id="fullname"
                                                name="fullname"
                                                value={this.state.fullname || ''}
                                                onChange={this.handleNameChange} />
                                            <label htmlFor='specialty'>Specialty</label>
                                            <input type="text"
                                                className='form-control'
                                                id="specialty"
                                                name="specialty"
                                                value={this.state.specialty || ''}
                                                onChange={this.handleSpecialtyChange} />
                                            <label htmlFor='graduationyear'>Graduation Year</label>
                                            <input type="text"
                                                className='form-control'
                                                id="graduationyear"
                                                name="graduationyear"
                                                value={this.state.graduationyear || ''}
                                                onChange={this.handleGraduationYearChange} />
                                        </div>
                                    </div>
                                </div>
                                {!this.state.editDisabled ? (
                                    <button type="submit"
                                        className="btn btn-success btn-blockk"
                                        onClick={this.onSubmit.bind(this)}>
                                        Submit
                    </button>
                                ) : (
                                        ''
                                    )}
                                {this.state.editDisabled ? (
                                    <button type="submit"
                                        onClick={this.onUpdate.bind(this)}
                                        className="btn btn-primary btn-blockk">
                                        Update
                    </button>
                                ) : (
                                        ''
                                    )}
                            </form>
                            <br/>
                            <div className="table-color">
                                <table className="table">
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Specialty</th>
                                        <th>Graduation Date</th>
                                    </tr>
                                    <tbody>
                                        {this.state.items.map((item, index) => (
                                            <tr key={index}>
                                                <td className="text-left">{item.fullname} </td>
                                                <td className="text-left">{item.specialty} </td>
                                                <td className="text-left">{item.graduationyear} </td>
                                                <td className="text-right">
                                                    <button
                                                        href=""
                                                        className="btn btn-info mr-1"
                                                        disabled={this.state.editDisabled}
                                                        onClick={this.onEdit.bind(
                                                            this,
                                                            item.doctorid
                                                        )}>
                                                        Edit
                                    </button>
                                                    <button
                                                        href=""
                                                        className="btn btn-danger"
                                                        disabled={this.state.editDisabled}
                                                        onClick={this.onDelete.bind(
                                                            this,
                                                            item.doctorid
                                                        )}
                                                    >
                                                        Delete
                                    </button>
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                )
            }
            if (objectUser.userType === "simpleUser") {
                return <Redirect to='/user' />;
            }

        } else {
            return <Redirect to='*' />;
        }
    }
}

export default DoctorsCRUD;