import React, { Component } from 'react';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { Col } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Image from 'react-bootstrap/Image';
import ImageUpload from './ImageUpload';
import SweetAlert from '../../node_modules/react-bootstrap-sweetalert';
import { UncontrolledAlert } from 'reactstrap';

const imgMyimageexample = require('../images/background/doctor1.jpg');
const divStyle = {
    margin: '0',
    height: '100%',
    backgroundImage: `url(${imgMyimageexample})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat'
};

class UserPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userID: "",
            name: "",
            email: "",
            password: "",
            disease: "",
            nameValid: false,
            emailValid: false,
            passwordValid: false,
            submitDisable: true,
            diseases: [],
            alert: null


        };
        console.log("This is constructor in UserPage");
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
        console.log(this.state);
    }

    handleNameChange = event => {
        let nameValid = event.target.value ? true : false;
        let submitValid = this.state.passwordValid && nameValid;
        this.setState({
            nameValid: nameValid,
            submitDisable: !submitValid,
            name: event.target.value
        });
    }
    handleEmailChange = event => {
        let emailValid = event.target.value ? true : false;
        let submitValid = this.state.passwordValid && emailValid;
        // event.target returns the element that triggered the event

        this.setState({
            emailValid: emailValid,
            submitDisable: !submitValid,
            email: event.target.value
        });
        console.log(this.state);

    };

    handlePasswordChange = event => {
        let passwordValid = event.target.value ? true : false;
        let submitValid = this.state.emailValid && passwordValid;
        // event.target returns the element that triggered the event

        this.setState({
            passwordValid: passwordValid,
            submitDisable: !submitValid,
            password: event.target.value
        });
        console.log(this.state);

    };


    handleSubmit = (event) => {
        //exista un event default care poate declansa onSubmit, iar noi nu vrem asta
        event.preventDefault();

        const updatedUser = axios
            .put("http://localhost/cosmina/updateUser.php", this.state) //trimit in backend datele
            .then(
                (response) => {
                    console.log("updated user", response.data);//vei primi rezultatele de la backend daca acestea au fost trimise
                    console.log("date schimbate", response.data.name, response.data.email, response.data.password);

                }
            )
            .catch(function (error) {
                console.log(error);
            });
        console.log(updatedUser);

        const user = axios
            .post("http://localhost/cosmina/user.php", this.state) //trimit in backend datele
            .then((response) => {

                console.log("user details", response.data);
                if (response.data) {
                    sessionStorage.setItem('userData', JSON.stringify(response.data));
                }
                else {
                    console.log("Could not retrieve user");
                    //this.deleteThisGoal();

                }
            })
            .catch(function (error) {
                console.log(error);
            });
        console.log(user);
        //this.componentDidUpdate(this.props, this.state);
    };

    async componentDidMount() {
        console.log("Comp did mount");
        const result = await axios.get("http://localhost/cosmina/diseases.php");
        console.log("Array of diseases:", result.data);
        this.setState({
            diseases: result.data //populam state-ul cu ~ v[i] elem curent, din backend
        });
        for (const key in this.state.diseases) {
            let value = this.state.diseases[key];
            console.log(value.disease);
        }
        Object.entries(this.state.diseases).forEach(entry => {
            let d1 = entry[1].disease;

            //use key and value here
        });
        console.log("updated user to: ");
        const response = await axios
            .get('http://localhost/cosmina/user.php');
        this.setState({
            name: response.data.name,
            email: response.data.email,
            password: response.data.password
        });

        console.log("response ", response);

        if (sessionStorage.getItem("userData")) {
            this.deleteThisGoal();

        }

    }

    deleteThisGoal = () => {
        console.log("Delete goal");
        const getAlert = () => (
            <SweetAlert
                success
                title="Woot!"
                onConfirm={() => this.hideAlert()}
            >
                Hello world!
          </SweetAlert>
            // <AlertDismissible/>
        );

        this.setState({
            alert: getAlert()
        });
    }

    getDiseases = () => {
        for (const key in this.state.diseases) {
            let value = this.state.diseases[key];
            console.log(value.disease);
            return value.disease;
        }

    }

    componentDidUpdate = (prevProps, prevState) => {
        console.log("Comp did update!");
        let userString = sessionStorage.getItem('userData');
        console.log('Storage string', userString);
        if (userString) {
            let obj = JSON.parse(userString);
            if (!prevState.email && !prevState.password && !prevState.userID && !prevState.name && !prevState.disease) {
                this.setState({
                    userID: obj.userID,
                    name: obj.name,
                    email: obj.email,
                    password: obj.password,
                    disease: obj.disease
                })
                console.log("Userstate is", this.state);
            }
        }
    }

    parseUserStringToObject = () => {
        let userString = sessionStorage.getItem('userData');
        if (userString) {
            let obj = JSON.parse(userString);
            console.log("String parsed successfully", obj);
            console.log(obj.userID, obj.name, obj.email, obj.password, obj.userType);
            return obj;
        }

    }


    render() {
        if (sessionStorage.getItem('userData')) {
            let obj = this.parseUserStringToObject();
            this.componentDidUpdate(this.props, this.state);
            let nr = this.state.userID;
            let url = localStorage.getItem("profilePicture" + nr);
            return <div className="page-container" style={divStyle}>
                <UncontrolledAlert color="success">
                    Login successful!
                </UncontrolledAlert>
                <Form onSubmit={this.handleSubmit} className="user-form">
                    <Form.Row>
                        <div className="image-upload-wrapper">
                            <Image src={url || 'http://via.placeholder.com/280x300'}
                                rounded
                                className='profile-picture'
                                alt="profile image"
                            />
                            <Form.Row>
                                <ImageUpload userID={this.state.userID} />
                            </Form.Row>
                        </div>

                        <div className="form-input">
                            <Form.Row>
                                <Form.Group as={Col} md="12" controlId="formUserName">
                                    <Form.Label>Full Name</Form.Label>
                                    <Form.Control name="name" type="name" placeholder={obj.name} onChange={this.handleNameChange} />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} md="12" controlId="formUserEmail">
                                    <Form.Label>Email address</Form.Label>
                                    <Form.Control name="email" type="email" placeholder={obj.email} onChange={this.handleEmailChange} /> {/*this is input*/}
                                    <Form.Text className="text-muted">
                                        We'll never share your email with anyone else.
                                    </Form.Text>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} md="12" controlId="formUserPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control name="password" type="password" placeholder={obj.password} onChange={this.handlePasswordChange} />{/*this is input*/}
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} md="12" controlId="formUserDisease">
                                    <Form.Label>Disease</Form.Label>
                                    <Form.Control name="diseases" as="select" className='select-size' onChange={this.handleChange}>
                                        <option>{this.state.disease}</option>
                                        <option>daltonism</option>
                                        <option>hernie de disc</option>
                                        <option>herpes</option>

                                    </Form.Control>
                                </Form.Group>
                            </Form.Row>
                            <Button variant="primary" type="submit" disabled={this.state.submitDisable} className="edit-button" >
                                Change your data
                                </Button>
                        </div>
                    </Form.Row>
                </Form>
            </div>
        } else {
            return <Redirect to='/login' />;
        }

    }

}

export default UserPage;