import firebase from 'firebase/app';
import 'firebase/storage';

var config = {
    apiKey: "AIzaSyAFCLDiAx2j8dhVyBuzDW8v9VoXxzUK01E",
    authDomain: "tema2-6b0b4.firebaseapp.com",
    databaseURL: "https://tema2-6b0b4.firebaseio.com",
    projectId: "tema2-6b0b4",
    storageBucket: "tema2-6b0b4.appspot.com",
    messagingSenderId: "214559460643"
  };

firebase.initializeApp(config);

const storage=firebase.storage();

export {
    storage, firebase as default
}