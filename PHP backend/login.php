<?php
include 'functions.php';

if (enable_cors()) {
    return;
}

$requestBody = json_decode(file_get_contents('php://input'));

$con = connect_to_db();

$email = $requestBody->email;
$password = $requestBody->password;

$query=
"select u.userID, ud.name, u.email, u.password, ud.disease, u.userType, ud.latitude, ud.longitude
from user u left join userdetails ud on u.userID=ud.fkUserID
WHERE u.email='$email' AND u.password='$password'";

if ($result = mysqli_query($con, $query)) {
    // Fetch one and one row
    $user = mysqli_fetch_object($result);
    // Free result set
    mysqli_free_result($result);
}

mysqli_close($con);

echo json_encode($user);
