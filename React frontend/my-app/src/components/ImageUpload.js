import React, { Component } from 'react';
import { storage } from '../firebase';
import ProgressBar from 'react-bootstrap/ProgressBar';
import Button from 'react-bootstrap/Button';

class ImageUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null,
      url: '',
      progress: 0
    }
    this.handleChange = this
      .handleChange
      .bind(this);
    this.handleUpload = this.handleUpload.bind(this);
  }
  handleChange = e => {
    if (e.target.files[0]) {
      const image = e.target.files[0];
      this.setState(() => ({ image }));
    }
  }
  handleUpload = () => {
    const { image } = this.state;
    const uploadTask = storage.ref(`images/${image.name}`).put(image);
    uploadTask.on('state_changed',
      (snapshot) => {
        // progrss function ....
        const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        this.setState({ progress });
      },
      (error) => {
        // error function ....
        console.log(error);
      },
      () => {
        // complete function ....
        storage.ref('images').child(image.name).getDownloadURL().then(url => {
          console.log(url);
          this.setState({ url });
          let nr = this.props.userID;
          console.log("user id after stringify", nr);
          localStorage.setItem("profilePicture" + nr, url);
        })
      });
  }
  render() {
    const style = {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center'
    };

    return (
      <div style={style}>
        <br />
        <input type="file" onChange={this.handleChange} />
        <br />
        <Button type="submit" className="upload-button" onClick={this.handleUpload}>
          Upload photo
                </Button>
        <br />
        <ProgressBar variant="warning" now={this.state.progress} />
        <br/>
      </div>
    )
  }
}

export default ImageUpload;