<?php
function enable_cors()
{
    header('Access-Control-Allow-Origin: http://localhost:3000');
    header('Access-Control-Allow-Headers: Content-Type');
    header('Access-Control-Allow-Methods: POST, PUT, GET, OPTIONS');
    $value = getenv('REQUEST_METHOD');


    if($value==="OPTIONS"){

        echo 'CORS enabled for localhost:3000';
        return true;

    }
    return false;
}

function connect_to_db()
{
    $con = mysqli_connect("localhost", "root", "", "cosmina");
    // Check connection
    if (mysqli_connect_errno()) {
          throw new Error("Failed to connect to MySQL: " . mysqli_connect_error());
    }
    return $con;
}