import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import { UncontrolledAlert } from 'reactstrap';


const imgMyimageexample = require('../images/background/doctor1.jpg');
const divStyle = {
    margin: '0',
    height: '100%',
    backgroundImage: `url(${imgMyimageexample})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat'
};

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            userType: "",
            latitude: "",
            longitude: "",
            fkUserId: '',
            redirectUser: false,
            emailValid: false,
            passwordValid: false,
            submitDisable: true,
            alert: null,
            loginUnsuccessful: "0" //stare initiala in care nu am apasat pe buton
        };
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
        console.log(this.state);
    }
    handleEmailChange = event => {
        let emailValid = event.target.value ? true : false;
        let submitValid = this.state.passwordValid && emailValid;
        // event.target returns the element that triggered the event

        this.setState({
            emailValid: emailValid,
            submitDisable: !submitValid,
            email: event.target.value
        });
        console.log(this.state);

    };

    handlePasswordChange = event => {
        let passwordValid = event.target.value ? true : false;
        let submitValid = this.state.emailValid && passwordValid;
        // event.target returns the element that triggered the event

        this.setState({
            passwordValid: passwordValid,
            submitDisable: !submitValid,
            password: event.target.value
        });
        console.log(this.state);

    };

    async getHospitals() {
        const hospitals = await axios
            .post("http://localhost/cosmina/map.php", this.state) //trimit in backend datele
            .then((response) => {

                console.log("date din backend map click", response.data);
                sessionStorage.setItem("hospitals", JSON.stringify(response.data));

            })
            .catch(function (error) {
                console.log(error);
            });
        console.log(hospitals);
    }

    handleSubmit = (event) => {
        //exista un event default care poate declansa onSubmit, iar not nu vrem asta
        event.preventDefault();

        const user = axios
            .post("http://localhost/cosmina/login.php", this.state) //trimit in backend datele
            .then((response) => {

                console.log("date din backend", response.data);
                if (response.data) {
                    sessionStorage.setItem('userData', JSON.stringify(response.data));
                    let user = sessionStorage.getItem('userData');
                    var objectUser = JSON.parse(user);
                    var fkUserId = objectUser.userID;
                    console.log("This is user role ", objectUser.userType);
                    this.setState({
                        fkUserId: fkUserId,
                        redirectUser: true,
                        userType: objectUser.userType,
                        loginUnsuccessful: "1"
                    });

                    const hospitals = axios
                        .post("http://localhost/cosmina/map.php", this.state) //trimit in backend datele
                        .then((response) => {

                            console.log("date din backend map click", response.data);
                            sessionStorage.setItem("hospitals", JSON.stringify(response.data));

                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    console.log(hospitals);

                }
                else {
                    console.log("Login Unsuccessful!");
                    this.setState({
                        loginUnsuccessful: "2"
                    });
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        console.log(user);
        //this.getHospitals();

    };




    // componentDidUpdate(prevProps, prevState) { 
    //     if (prevState.redirectUser === false && this.state.redirectUser === true) {
    //         this.setState({redirectUser: false});
    //      }
    // } 


    render() {
        if (this.state.redirectUser) {
            if (this.state.userType === "simpleUser") {
                return <Redirect to='/user' />;
            }
            if (this.state.userType === "admin") {
                return <Redirect to='/admin' />;
            }
            else {
                return <Redirect to='*' />;
            }
        }
        if (this.state.loginUnsuccessful === "2") { //fara success
            return (
                <div>
                    <UncontrolledAlert color="danger">
                        Login Unsuccessful!
                    </UncontrolledAlert>

                    <div className="page-container" style={divStyle}>
                        <Form onSubmit={this.handleSubmit} className="center-form">
                            <Form.Row>
                                <Form.Group as={Col} md="3" controlId="formBasicEmail" className='add-margin-to-form'>
                                    <Form.Label>Email address</Form.Label>
                                    <Form.Control
                                        name="email"
                                        type="email"
                                        placeholder="Enter email"
                                        onChange={this.handleEmailChange} />
                                    <Form.Text className="text-muted">
                                        We'll never share your email with anyone else.
                                    </Form.Text>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} md="3" controlId="formBasicPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control
                                        name="password"
                                        type="password"
                                        placeholder="Password"
                                        onChange={this.handlePasswordChange} />
                                </Form.Group>
                            </Form.Row>
                            <Button variant="primary" type="submit" disabled={this.state.submitDisable} className="login-button">
                                Login
                            </Button>
                            <br /><br />
                            <a href="http://localhost:3000/register">Don't have an account? Register...</a>
                            <br /><br />

                        </Form>
                    </div>
                </div>
            )
        }
        if (this.state.loginUnsuccessful === "0") {
            // nu am apasat pe buton
            return (
                <div className="page-container" style={divStyle}>
                    <Form onSubmit={this.handleSubmit} className="center-form">
                        <Form.Row>
                            <Form.Group as={Col} md="3" controlId="formBasicEmail" className='add-margin-to-form'>
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                    name="email"
                                    type="email"
                                    placeholder="Enter email"
                                    onChange={this.handleEmailChange} />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                </Form.Text>
                            </Form.Group>
                        </Form.Row>
                        <Form.Row>
                            <Form.Group as={Col} md="3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                    name="password"
                                    type="password"
                                    placeholder="Password"
                                    onChange={this.handlePasswordChange} />
                            </Form.Group>
                        </Form.Row>
                        <Button variant="primary" type="submit" disabled={this.state.submitDisable} className="login-button">
                            Login
                </Button>
                        <br /><br />
                        <a href="http://localhost:3000/register">Don't have an account? Register...</a>
                        <br /><br />

                    </Form>
                </div>
            )
        }

    }


}


export default Login;