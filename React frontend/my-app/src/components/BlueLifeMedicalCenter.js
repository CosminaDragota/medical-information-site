import React, { Component } from 'react';
import Image from 'react-bootstrap/Image';
import PuiaCosmin from '../images/puia_cosmin.png';
import RaduHorea from '../images/radu_horea.jpg';
import StancaMarius from '../images/stanca_marius.png';
import DanaBartos from '../images/danabartos2.jpg';
import FurceaLuminita from '../images/furcea_luminita.png';
import axios from 'axios';

const Doctor = (props) => (
  <div>
    <div class="container">
      <Image src={props.profilePicture} className="scale-with-grid" alt="Avatar" class="image" />
      <div class="overlay">
        <div class="text">{props.specialty}</div>
      </div>
    </div>
    <h2>{props.fullName}</h2>
    <p>{props.graduationYear}</p>
  </div>
);
const Doctor1 = (props) => (
  <div class="overlay">
    <div class="text">{props.specialty}</div>
  </div>
);

const Doctor2 = (props) => (
  <div>
    <h2>{props.fullName}</h2>
    <h4>Data absolvirii: </h4><p>{props.graduationYear}</p>

  </div>
)
class BlueLifeMedicalCenter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctors: [] //in stare am acest array
    };
  }

  //toate rest callurile se fac aici
  async componentDidMount() {
    const result = await axios.get("http://localhost/cosmina/bluelife.php");
    this.setState({
      doctors: result.data //populam state-ul cu ~ v[i] elem curent, din backend
    });
  }
    render() {
      return (
        <div>
        <div className="medsan-container">
          <h2>Medsan</h2>
          <div class="row">
            <div class="column" >
              <div class="container">
                <Image src={DanaBartos} className="scale-with-grid" alt="Avatar" class="image" />
                {this.state.doctors.filter(i => i.doctorID == 16).map(i => <Doctor1 {...i} />)}
              </div>
              {this.state.doctors.filter(i => i.doctorID == 16).map(i => <Doctor2 {...i} />)}
            </div>

            <div class="column" >
              <div class="container">
                <Image src={FurceaLuminita} className="scale-with-grid" alt="Avatar" class="image" />
                {this.state.doctors.filter(i => i.doctorID == 17).map(i => <Doctor1 {...i} />)}
              </div>
              {this.state.doctors.filter(i => i.doctorID == 17).map(i => <Doctor2 {...i} />)}
            </div>

            <div class="column" >
              <div class="container">
                <Image src={StancaMarius} className="scale-with-grid" alt="Avatar" class="image" />
                {this.state.doctors.filter(i => i.doctorID == 18).map(i => <Doctor1 {...i} />)}
              </div>
              {this.state.doctors.filter(i => i.doctorID == 18).map(i => <Doctor2 {...i} />)}

            </div>
          </div>
        </div>
      </div>
      //   <div>
      //   <div className="medsan-container">
      //     <h2>Bluelife</h2>
      //     <div class="row">
      //       <div class="column" >
      //         {this.state.doctors.map(i => <Doctor {...i} />)} 
      //         {/*pt fiecare element din state, vom randa un nou doctor cu propr coresp din state */}
      //       </div>
      //     </div>
      //   </div>
      // </div>
      ////////////////////////////
      //   <div className="medsan-container">
      //   <h2>Blue Life Medical Center</h2>
      //   <div class="row">
      //     <div class="column" >
      //       <div class="container">
      //         <Image src={PuiaCosmin} className="scale-with-grid" alt="Avatar" class="image" />
      //         <div class="overlay">
      //           <div class="text">medic homeopat</div>
      //         </div>
      //       </div>
      //       DR. PUIA COSMIN
      //     <p>An absolvire: 2006</p>
      //     </div>
      //     <div class="column" >
      //       <div class="container">
      //         <Image src={RaduHorea} className="scale-with-grid" alt="Avatar" class="image" />
      //         <div class="overlay">
      //           <div class="text">medic homeopat</div>
      //         </div>
      //       </div>
      //       DR.  RADU HOREA
      //     <p>An absolvire: 2005</p>
      //     </div>
      //     <div class="column" ><div class="container">
      //       <Image src={StancaMarius} className="scale-with-grid" alt="Avatar" class="image" />
      //       <div class="overlay">
      //         <div class="text">medic psihoterapeut</div>
      //       </div>
      //     </div>
      //       DR. STANCA MARIUS
      //     <p>An absolvire: 2010</p>
      //     </div>
      //   </div>
      // </div>
  
      )
    }
  }
  export default BlueLifeMedicalCenter;