import React, { Component } from 'react';
import Image from 'react-bootstrap/Image';
import CotaCristian from '../images/IMG_1986.jpg';
import DragotaTudor from '../images/IMG_1999.jpg';
import AlexStanciu from '../images/IMG_2009.jpg';
import DanaBartos from '../images/danabartos2.jpg';

import axios from 'axios';

const Doctor = (props) => (
  <div>
    <div class="container">
      <Image src={props.profilePicture} className="scale-with-grid" alt="Avatar" class="image" />
      <div class="overlay">
        <div class="text">{props.specialty}</div>
      </div>
    </div>
    <h2>{props.fullName}</h2>
    <p>{props.graduationYear}</p>
  </div>
);

const Doctor1 = (props) => (
  <div class="overlay">
    <div class="text">{props.specialty}</div>
  </div>
);

const Doctor2 = (props) => (
  <div>
    <h2>{props.fullName}</h2>
    <h4>Data absolvirii: </h4><p>{props.graduationYear}</p>

  </div>
);
class CentrulMedicalAsteco extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctors: [] //in stare am acest array
    };
  }

  //toate rest callurile se fac aici
  async componentDidMount() {
    const result = await axios.get("http://localhost/cosmina/asteco.php");
    this.setState({
      doctors: result.data //populam state-ul cu ~ v[i] elem curent, din backend
    });
  }
  render() {
      return (
        <div>
        <div className="medsan-container">
          <h2>Medsan</h2>
          <div class="row">
            <div class="column" >
              <div class="container">
                <Image src={CotaCristian} className="scale-with-grid" alt="Avatar" class="image" />
                {this.state.doctors.filter(i => i.doctorID == 7).map(i => <Doctor1 {...i} />)}
              </div>
              {this.state.doctors.filter(i => i.doctorID == 7).map(i => <Doctor2 {...i} />)}
            </div>

            <div class="column" >
              <div class="container">
                <Image src={DragotaTudor} className="scale-with-grid" alt="Avatar" class="image" />
                {this.state.doctors.filter(i => i.doctorID == 8).map(i => <Doctor1 {...i} />)}
              </div>
              {this.state.doctors.filter(i => i.doctorID == 8).map(i => <Doctor2 {...i} />)}
            </div>

            <div class="column" >
              <div class="container">
                <Image src={AlexStanciu} className="scale-with-grid" alt="Avatar" class="image" />
                {this.state.doctors.filter(i => i.doctorID == 9).map(i => <Doctor1 {...i} />)}
              </div>
              {this.state.doctors.filter(i => i.doctorID == 9).map(i => <Doctor2 {...i} />)}

            </div>
          </div>
        </div>
      </div>
      //   <div>
      //   <div className="medsan-container">
      //     <h2>Asteco</h2>
      //     <div class="row">
      //       <div class="column" >
      //         {this.state.doctors.map(i => <Doctor {...i} />)} 
      //         {/*pt fiecare element din state, vom randa un nou doctor cu propr coresp din state */}
      //       </div>
      //     </div>
      //   </div>
      // </div>
      ////////
        // <div className="medsan-container">
        //   <h2>Centrul Medical Asteco</h2>
        //   <div class="row">
        //     <div class="column" >
        //       <div class="container">
        //         <Image src={CotaCristian} className="scale-with-grid" alt="Avatar" class="image" />
        //         <div class="overlay">
        //           <div class="text">medic ginecolog</div>
        //         </div>
        //       </div>
        //       DR. COTA CRISTIAN
        //     <p>An absolvire: 2010</p>
        //     </div>
        //     <div class="column" >
        //       <div class="container">
        //         <Image src={DragotaTudor} className="scale-with-grid" alt="Avatar" class="image" />
        //         <div class="overlay">
        //           <div class="text">medic de familie</div>
        //         </div>
        //       </div>
        //       DR. DRAGOTA TUDOR
        //     <p>An absolvire: 2011</p>
        //     </div>
        //     <div class="column" ><div class="container">
        //       <Image src={AlexStanciu} className="scale-with-grid" alt="Avatar" class="image" />
        //       <div class="overlay">
        //         <div class="text">medic homeopat</div>
        //       </div>
        //     </div>
        //       DR. STANCIU ALEXANDRU
        //     <p>An absolvire: 2001</p>
        //     </div>
        //   </div>
        // </div>
  
      )
    }
  }

  export default CentrulMedicalAsteco;