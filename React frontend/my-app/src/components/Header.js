import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import '../App.css';
import t from '../translate/Locale';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import style from 'bootstrap/dist/css/bootstrap.css';

class Header extends Component {
    render() {
        return (
            <Navbar bg="dark" variant="dark" expand="lg">
                <Navbar.Brand href="/" activeClassName="is-active" >{t('Acasa')}</Navbar.Brand>
                <Nav className="mr-auto">
                    <Link to={'/unitati'} className="nav-link">{t('Unitati')}</Link>
                    <Link to={'/medici'} className="nav-link">{t('Medici')}</Link>
                    <Link to={'/admin'} className="nav-link">{t('Admin')}</Link>
                    <Link to={'/statistics'} className="nav-link">{t('Statistics')}</Link>
                    <Link to={'/map'} className="nav-link">{t('Map')}</Link>

                </Nav>
                <Link to={'/'} className="nav-link"
                    onClick={() => {
                        sessionStorage.clear();
                    }}
                >
                    <Button variant="outline-info">
                        {t('Logout')}
                    </Button>
                </Link>
                <Link to={'/login'} className="nav-link">
                    <Button variant="outline-info">
                        {t('Login')}
                    </Button>
                </Link>


                <Button variant="outline-info" className="fit-button"
                    onClick={() => {
                        localStorage.setItem('locale', 'en');
                        window.location.reload();
                    }
                    }>
                    EN</Button>

                <Button variant="outline-info"
                    onClick={() => {
                        localStorage.setItem('locale', 'ro');
                        window.location.reload();
                    }}>
                    RO</Button>
            </Navbar>
        );
    }
}

export default Header;