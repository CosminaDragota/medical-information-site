import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

class Footer extends Component {
    render() {
        return (
            <footer className="footer-color">
                <Row >
                    <Col lg={6}>
                        <p>Posted by: Cosmina Dragota</p>

                    </Col>
                    <Col lg={6} style={{textAlign:'right'}}>
                        <p>All rights reserved.</p>

                    </Col>
                </Row>
            </footer>
        );
    }
}

export default Footer;