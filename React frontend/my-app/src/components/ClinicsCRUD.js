import React, { Component } from 'react';
import { getList, addItem, deleteItem, updateItem } from './ClinicsCRUDFunctions';
import { Redirect } from 'react-router-dom';

const imgMyimageexample = require('../images/background/doctor1.jpg');
const divStyle = {
    margin: '0',
    height: '100%',
    backgroundImage: `url(${imgMyimageexample})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat'
};

class ClinicsCRUD extends Component {
    constructor() {
        super()
        this.state = {
            medicalunitid: '',
            unittype: '',
            name: '',
            location: '',
            logo: '',
            editDisabled: false,
            items: []
        }
    }

    componentDidMount() {
        this.getAll()
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleUnitTypeChange = e => {
        this.setState({
            unittype: e.target.value
        })
    }

    handleNameChange = e => {
        this.setState({
            name: e.target.value
        })
    }

    handleLocationChange = e => {
        this.setState({
            location: e.target.value
        })
    }

    getAll = () => {
        getList().then(data => {
            this.setState({
                unittype: '',
                name: '',
                location: '',
                logo: '',
                items: [...data]
            },
                () => {
                    console.log(this.state.items)
                })
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        addItem(this.state.unittype, this.state.name, this.state.location, this.state.logo).then(() => {
            this.getAll()
        })
        this.setState({
            unittype: '',
            name: '',
            location: ''
            // logo: '',
        })
    }

    onUpdate = (e) => {
        e.preventDefault()
        updateItem(
            this.state.unittype, this.state.name, this.state.location, this.state.logo, this.state.medicalunitid
        ).then(() => {
            this.getAll()
        })
        this.setState({
            unittype: '',
            name: '',
            location: '',
            // logo: '',
            editDisabled: ''
        })
        this.getAll()
    }

    onEdit = (itemid, e) => {
        e.preventDefault();

        sessionStorage.setItem('clinic', JSON.stringify(this.state));
        console.log("On edit", itemid);

        var data = [...this.state.items]
        data.forEach((item, index) => {
            if (item.medicalunitid === itemid) {
                console.log("true");
                this.setState({
                    medicalunitid: item.medicalunitid,
                    unittype: item.unittype,
                    name: item.name,
                    location: item.location,
                    //logo: item.logo,
                    editDisabled: true
                })
            }
        })
    }

    onDelete = (val, e) => {
        e.preventDefault();
        deleteItem(val);
        this.getAll();
    }

    render() {
        let user = sessionStorage.getItem('userData');
        let objectUser = JSON.parse(user);
        if (user) {

            if (objectUser.userType === "admin") {

                return (
                    <div className="page-container-links" style={divStyle}>
                        <div className="col-md-12">
                            <form onSubmit={this.onSubmit}>
                                <div className='form-group'>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <label htmlFor='fullname'>Clinic Type</label>
                                            <input type="text"
                                                className='form-control'
                                                id="fullname"
                                                name="fullname"
                                                value={this.state.unittype || ''}
                                                onChange={this.handleUnitTypeChange} />
                                            <label htmlFor='specialty'>Clinic Name</label>
                                            <input type="text"
                                                className='form-control'
                                                id="specialty"
                                                name="specialty"
                                                value={this.state.name || ''}
                                                onChange={this.handleNameChange} />
                                            <label htmlFor='graduationyear'>Clinic Location</label>
                                            <input type="text"
                                                className='form-control'
                                                id="graduationyear"
                                                name="graduationyear"
                                                value={this.state.location || ''}
                                                onChange={this.handleLocationChange} />
                                        </div>
                                    </div>
                                </div>
                                {!this.state.editDisabled ? (
                                    <button type="submit"
                                        className="btn btn-success btn-blockk"
                                        onClick={this.onSubmit.bind(this)}>
                                        Submit
                    </button>
                                ) : (
                                        ''
                                    )}
                                {this.state.editDisabled ? (
                                    <button type="submit"
                                        onClick={this.onUpdate.bind(this)}
                                        className="btn btn-primary btn-blockk">
                                        Update
                    </button>
                                ) : (
                                        ''
                                    )}
                            </form>
                            <br/>
                            <div className="table-color">
                            <table className="table">
                            <tr>
                                    <th>Clinic Type</th>
                                    <th>Clinic Name</th>
                                    <th>Clinic Location</th>
                                </tr>
                                <tbody>
                                    {this.state.items.map((item, index) => (
                                        <tr key={index}>
                                            <td className="text-left">{item.unittype} </td>
                                            <td className="text-left">{item.name} </td>
                                            <td className="text-left">{item.location} </td>
                                            <td className="text-right">
                                                <button
                                                    href=""
                                                    className="btn btn-info mr-1"
                                                    disabled={this.state.editDisabled}
                                                    onClick={this.onEdit.bind(
                                                        this,
                                                        item.medicalunitid
                                                    )}>
                                                    Edit
                                    </button>
                                                <button
                                                    href=""
                                                    className="btn btn-danger"
                                                    disabled={this.state.editDisabled}
                                                    onClick={this.onDelete.bind(
                                                        this,
                                                        item.medicalunitid
                                                    )}
                                                >
                                                    Delete
                                    </button>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                            </div>

                        </div>
                    </div>
                )
            }
            if (objectUser.userType === "simpleUser") {
                return <Redirect to='/user' />;
            }

        } else {
            return <Redirect to='*' />;
        }
    }
}

export default ClinicsCRUD;