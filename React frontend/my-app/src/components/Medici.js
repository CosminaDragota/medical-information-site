import React, { Component } from 'react';
import Image from 'react-bootstrap/Image';
import DanaBartos from '../images/danabartos2.jpg';
import FurceaLuminita from '../images/furcea_luminita.png';
import GraurFlorin from '../images/graur_florin.png';
import IancuCornel from '../images/iancu-cornel.jpg';
import FelecanAda from '../images/IMG_1975.jpg';
import VidaRadu from '../images/IMG_1939.jpg';
import CotaCristian from '../images/IMG_1986.jpg';
import DragotaTudor from '../images/IMG_1999.jpg';
import AlexStanciu from '../images/IMG_2009.jpg';
import MocanLucian from '../images/mocan_lucian.png';
import MolnarGeza from '../images/molnar_geza.png';
import MunteanuDoru from '../images/munteanu_doru.png';
import MuresanAnca from '../images/muresan_anca.png';
import NeculaAlexandru from '../images/necula_alexandru.png';
import OsianGelu from '../images/osian_gelu.png';
import PuiaCosmin from '../images/puia_cosmin.png';
import RaduHorea from '../images/radu_horea.jpg';
import StancaMarius from '../images/stanca_marius.png';
import ZaharieFlorin from '../images/zaharie_florin.png';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import CardGroup from 'react-bootstrap/CardGroup';

import Medsan from './Medsan';
import PoliclinicaMedstarManastur from './PoliclinicaMedstarManastur';
import CentrulMedicalAsteco from './CentrulMedicalAsteco';
import NewMedica from './NewMedica';
import HyperclinicaMedlife from './HyperclinicaMedlife';
import BlueLifeMedicalCenter from './BlueLifeMedicalCenter';
import CabinetMedicalTrimed from './CabinetMedicalTrimed';
import PoliclinicaPromedis from './PoliclinicaPromedis';
import MedicoverRepublicii from './MedicoverRepublicii';

import axios from 'axios';

const Doctor = (props) => (
  <div>
    <div class="container">
      <Image src={props.profilePicture} className="scale-with-grid" alt="Avatar" class="image" />
      <div class="overlay">
        <div class="text">{props.specialty}</div>
      </div>
    </div>
    <h2>{props.fullName}</h2>
    <p>{props.graduationYear}</p>
  </div>
);

class Medici extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctors: [] //in stare am acest array
    };
  }

  //toate rest callurile se fac aici
  async componentDidMount() {
    const result = await axios.get("http://localhost/cosmina/doctors.php");
    this.setState({
      doctors: result.data //populam state-ul cu ~ v[i] elem curent, din backend
    });
  }

  render() {
    return (
      <div>
         <Medsan />
            <PoliclinicaMedstarManastur />
            <CentrulMedicalAsteco />
            <NewMedica />
            <HyperclinicaMedlife />
            <BlueLifeMedicalCenter />
            <CabinetMedicalTrimed />
            <PoliclinicaPromedis />
            <MedicoverRepublicii /> 
          
        {/* <div className="medsan-container">
          <h2>clinica</h2>
          <div class="row">
            <div class="column" >
              {this.state.doctors.map(i => <Doctor {...i} />)} 
              
            </div>
          </div>
        </div> */}

      </div>
    )
  }

}

export default Medici;