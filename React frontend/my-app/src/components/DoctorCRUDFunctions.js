import axios from 'axios';

export const getList = () => {
    return axios
        .get('api/doctors/', {
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => {
            return res.data
        })
}

export const addItem = (fullname,graduationyear, specialty, profilepicture) => {
    return axios
        .post('api/doctors/',
            {
                specialty:specialty,
                fullname:fullname,
                graduationyear:graduationyear,
                profilepicture:profilepicture
            },
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        .then( res =>{
            console.log(res)
        })
}

export const deleteItem = (doctorid) =>{
    return axios
    .delete(`api/doctors/${doctorid}`, {
        headers: { 'Content-Type': 'application/json' }

    })
    .then(res=>{
        console.log(res)
    })
    .catch(err=>{
        console.log(err)
    })
    
}

export const updateItem = (fullname,specialty,graduationyear,profilepicture,doctorid) =>{
    return axios
    .put(`api/doctors/${doctorid}/`,
    {
        doctorid:doctorid,
        specialty:specialty,
        fullname:fullname,
        graduationyear:graduationyear,
        profilepicture:profilepicture
    },
    {
        headers: { 'Content-Type': 'application/json' }

    })
    .then(res=>{
        console.log(res)
    })
    .catch(err=>{
        console.log(err)
    })
    
}