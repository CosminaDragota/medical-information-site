import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from './components/Home';
import Medici from './components/Medici';
import UnitatiMedicale from './components/UnitatiMedicale';
import Medsan from './components/Medsan';
import Medstar from './components/PoliclinicaMedstarManastur';
import Asteco from './components/CentrulMedicalAsteco';
import NewMedica from './components/NewMedica';
import Medlife from './components/HyperclinicaMedlife';
import BlueLife from './components/BlueLifeMedicalCenter';
import Trimed from './components/CabinetMedicalTrimed';
import Promedis from './components/PoliclinicaPromedis';
import Medicover from './components/MedicoverRepublicii';
import Header from './components/Header';
import Footer from './components/Footer';
import Login from './components/Login';
import UserPage from './components/UserPage';
import Register from './components/Register';
import NotFoundPage from './components/NotFoundPage';
import AdminManager from './components/AdminManager';
import DoctorsCRUD from './components/DoctorsCRUD';
import ClinicsCRUD from './components/ClinicsCRUD';
import UsersCRUD from './components/UsersCRUD';
import Statistics from './components/Statistics';
import GoogleApiWrapper from './components/Map';

class App extends Component {
  render() {
    return (
      <div>
      <Router>
        <div>
          <Header />

          <Switch>
            <Route exact={true} path='/' component={Home} />
            <Route path='/medici' component={Medici} />
            <Route path='/unitati' component={UnitatiMedicale} />
            <Route path='/medsan' component={Medsan} />
            <Route path='/medstar' component={Medstar} />
            <Route path='/asteco' component={Asteco} />
            <Route path='/newmedica' component={NewMedica} />
            <Route path='/medlife' component={Medlife} />
            <Route path='/bluelife' component={BlueLife} />
            <Route path='/trimed' component={Trimed} />
            <Route path='/promedis' component={Promedis} />
            <Route path='/medicover' component={Medicover} />
            <Route path='/login' component={Login} />
            <Route path='/register' component={Register} />
            <Route path='/user' component={UserPage} />
            <Route path='/admin' component={AdminManager} />
            <Route path='/doctorscrud' component={DoctorsCRUD}/>
            <Route path='/clinicscrud' component={ClinicsCRUD}/>
            <Route path='/userscrud' component={UsersCRUD}/>
            <Route path='/statistics'component={Statistics}/>
            <Route path='/map' component={GoogleApiWrapper}/>
            <Route path='*' component={NotFoundPage} />

          </Switch>
          <Footer/>
        </div>
      </Router>

      </div>

    );
  }
}

export default App;
