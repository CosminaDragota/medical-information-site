<?php
include 'functions.php';

if (enable_cors()) {
    return;
}

$requestBody = json_decode(file_get_contents('php://input'));

$con = connect_to_db();


	$sql="select ud.userDetailsID, ud.disease
	from  userdetails ud ";
	$diseases=[];
	if($result=mysqli_query($con, $sql))
	{
	//Fetch one and one row
		while($row=mysqli_fetch_object($result))
		{
			$diseases[]=$row;		
		}
		//Free result set
		mysqli_free_result($result);
	}


mysqli_close($con);

//Send data to front-end
echo json_encode($diseases);