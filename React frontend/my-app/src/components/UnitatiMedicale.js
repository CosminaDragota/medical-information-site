import React, { Component } from 'react';
import Table from 'react-bootstrap/Table';
import Image from 'react-bootstrap/Image';
import SiglaMedsan from '../images/unitati/medsan.png';
import Button from 'react-bootstrap/Button';
import Asteco from '../images/unitati/asteco.jpg';
import BlueLife from '../images/unitati/blue_life.jpg';
import Medicover from '../images/unitati/medicover.jpg';
import MedLife from '../images/unitati/medlife.jpg';
import Medstar from '../images/unitati/medstar.jpg';
import NewMedica from '../images/unitati/new_medica.jpg';
import Promedis from '../images/unitati/promedis.jpg';
import Trimed from '../images/unitati/trimed.jpg';
import t from '../translate/Locale';
import Card from 'react-bootstrap/Card';
import axios from 'axios';

const Clinica = (props) => (
  <div>
    <Card.Title>{props.name}</Card.Title>
    <Card.Text>
      {props.unitType}
      <br />
      {props.location}
    </Card.Text>
  </div>
);

class UnitatiMedicale extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clinics: [] //in stare am acest array
    };
  }

  //toate rest callurile se fac aici
  async componentDidMount() {
    const result = await axios.get("http://localhost/cosmina/clinics.php");
    this.setState({
      clinics: result.data //populam state-ul cu ~ v[i] elem curent, din backend
    });
  }
  render() {
    return (
      <div className="clinici-container">
        <h2 className="titluri">{t('Unitati')}</h2>
        <tr>
          <td>
            <Card style={{ width: '18rem' }}>
              <Card.Img variant="top" src={SiglaMedsan} thumbnail />
              <Card.Body>
                {/* <Card.Title>Medsan</Card.Title>
                  <Card.Text>
                  Centru Medical
                  <br/>
                  Strada Ilie Macelaru 28
                  </Card.Text>   */}
                <div>
                  {this.state.clinics.filter(i => i.medicalUnitID == 1).map(i => <Clinica {...i} />)}
                </div>
                <Button hover variant="outline-info" size="sm" className="pull-right"
                  onClick={this.raiseInvoiceClickedMedsan}
                >Informatii medici</Button>
              </Card.Body>
            </Card>
          </td>
          <td>
            <Card style={{ width: '18rem' }} className="card-indent">
              <Card.Img variant="top" src={Medstar} thumbnail />
              <Card.Body>
                {/* <Card.Title>Policlinica Medstar</Card.Title>
                  <Card.Text>
                  Clinica Medicala
                  <br/>
                  Strada Mehedinti 1-3
                  </Card.Text> */}
                {this.state.clinics.filter(i => i.medicalUnitID == 2).map(i => <Clinica {...i} />)}
                <Button hover variant="outline-info" size="sm" className="pull-right"
                  onClick={this.raiseInvoiceClickedMedstar}
                >Informatii medici</Button>
              </Card.Body>
            </Card>
          </td>
          <td>
            <Card style={{ width: '18rem' }} className="card-indent">
              <Card.Img variant="top" src={Asteco} thumbnail />
              <Card.Body>
                {/* <Card.Title>Centrul Medical Asteco</Card.Title>
                <Card.Text>
                  Servicii medicale
                  <br />
                  Strada Constantin Brancusi 105
                  </Card.Text> */}
                {this.state.clinics.filter(i => i.medicalUnitID == 3).map(i => <Clinica {...i} />)}

                <Button hover variant="outline-info" size="sm" className="pull-right"
                  onClick={this.raiseInvoiceClickedAsteco}
                >Informatii medici</Button>
              </Card.Body>
            </Card>
          </td>
          <td>
            <Card style={{ width: '18rem' }} className="card-indent">
              <Card.Img variant="top" src={NewMedica} thumbnail />
              <Card.Body>
                {/* <Card.Title>New Medica</Card.Title>
                <Card.Text>
                  Centru Medical
                  <br />
                  Strada Gheorghe Marinescu 6
                  </Card.Text> */}
                {this.state.clinics.filter(i => i.medicalUnitID == 4).map(i => <Clinica {...i} />)}

                <Button hover variant="outline-info" size="sm" className="pull-right"
                  onClick={this.raiseInvoiceClickedNewMedica}
                >Informatii medici</Button>
              </Card.Body>
            </Card>
          </td>
        </tr>
        <br />
        <tr>
          <td>
            <Card style={{ width: '18rem' }}>
              <Card.Img variant="top" src={MedLife} thumbnail />
              <Card.Body>
                {/* <Card.Title>Hyperclinica Medlife</Card.Title>
                <Card.Text>
                  Clinica Medicala
                  <br />
                  Strada Motilor 32
                  </Card.Text> */}
                {this.state.clinics.filter(i => i.medicalUnitID == 5).map(i => <Clinica {...i} />)}

                <Button hover variant="outline-info" size="sm" className="pull-right"
                  onClick={this.raiseInvoiceClickedMedlife}
                >Informatii medici</Button>
              </Card.Body>
            </Card>
          </td>
          <td>
            <Card style={{ width: '18rem' }} className="card-indent">
              <Card.Img variant="top" src={BlueLife} thumbnail />
              <Card.Body>
                {/* <Card.Title>Blue Life Medical Center</Card.Title>
                <Card.Text>
                  Centru Medical
                  <br />
                  Strada Motilor 32
                  </Card.Text> */}
                {this.state.clinics.filter(i => i.medicalUnitID == 6).map(i => <Clinica {...i} />)}
                <Button hover variant="outline-info" size="sm" className="pull-right"
                  onClick={this.raiseInvoiceClickedBluelife}
                >Informatii medici</Button>
              </Card.Body>
            </Card>
          </td>

          <td>
            <Card style={{ width: '18rem' }} className="card-indent">
              <Card.Img variant="top" src={Trimed} thumbnail />
              <Card.Body>
                {/* <Card.Title>Cabinet Medical Trimed</Card.Title>
                <Card.Text>
                  Centru Medical
                  <br />
                  Strada Aurel Vlaicu
                  </Card.Text> */}
                {this.state.clinics.filter(i => i.medicalUnitID == 7).map(i => <Clinica {...i} />)}

                <Button hover variant="outline-info" size="sm" className="pull-right"
                  onClick={this.raiseInvoiceClickedTrimed}
                >Informatii medici</Button>
              </Card.Body>
            </Card>
          </td>

          <td>
            <Card style={{ width: '18rem' }} className="card-indent">
              <Card.Img variant="top" src={Promedis} thumbnail />
              <Card.Body>
                {/* <Card.Title>Policlinica Promedis</Card.Title>
                <Card.Text>
                  Centru Medical
                  <br />
                  Strada Bucuresti 80
                  </Card.Text> */}
                {this.state.clinics.filter(i => i.medicalUnitID == 8).map(i => <Clinica {...i} />)}

                <Button hover variant="outline-info" size="sm" className="pull-right"
                  onClick={this.raiseInvoiceClickedPromedis}
                >Informatii medici</Button>
              </Card.Body>
            </Card>
          </td>
        </tr>
        <br />
        <tr>
          <td>
            <Card style={{ width: '18rem' }} >
              <Card.Img variant="top" src={Medicover} thumbnail />
              <Card.Body>
                {/* <Card.Title>Medicover Republicii</Card.Title>
                <Card.Text>
                  Clinica Medicala
                  <br />
                  Strada Republicii 75
                  </Card.Text> */}
                {this.state.clinics.filter(i => i.medicalUnitID == 9).map(i => <Clinica {...i} />)}

                <Button hover variant="outline-info" size="sm" className="pull-right"
                  onClick={this.raiseInvoiceClickedMedicover}
                >Informatii medici</Button>
              </Card.Body>
            </Card>
          </td>
        </tr>
      </div>
    );
  }

  raiseInvoiceClickedMedsan() {
    // your axios call here
    localStorage.setItem("pageData", "Data Retrieved from axios request");
    // route to new page by changing window.location
    window.open('http://localhost:3000/medsan', "_blank");

  }

  raiseInvoiceClickedMedstar() {
    // your axios call here
    localStorage.setItem("pageData", "Data Retrieved from axios request");
    // route to new page by changing window.location
    window.open('http://localhost:3000/medstar', "_blank");

  }

  raiseInvoiceClickedAsteco() {
    // your axios call here
    localStorage.setItem("pageData", "Data Retrieved from axios request");
    // route to new page by changing window.location
    window.open('http://localhost:3000/asteco', "_blank");

  }


  raiseInvoiceClickedNewMedica() {
    // your axios call here
    localStorage.setItem("pageData", "Data Retrieved from axios request");
    // route to new page by changing window.location
    window.open('http://localhost:3000/newmedica', "_blank");

  }
  raiseInvoiceClickedMedlife() {
    // your axios call here
    localStorage.setItem("pageData", "Data Retrieved from axios request");
    // route to new page by changing window.location
    window.open('http://localhost:3000/medlife', "_blank");
  }

  raiseInvoiceClickedBluelife() {
    // your axios call here
    localStorage.setItem("pageData", "Data Retrieved from axios request");
    // route to new page by changing window.location
    window.open('http://localhost:3000/bluelife', "_blank");

  }

  raiseInvoiceClickedTrimed() {
    // your axios call here
    localStorage.setItem("pageData", "Data Retrieved from axios request");
    // route to new page by changing window.location
    window.open('http://localhost:3000/trimed', "_blank");

  }
  raiseInvoiceClickedPromedis() {
    // your axios call here
    localStorage.setItem("pageData", "Data Retrieved from axios request");
    // route to new page by changing window.location
    window.open('http://localhost:3000/promedis', "_blank");

  }

  raiseInvoiceClickedMedicover() {
    // your axios call here
    localStorage.setItem("pageData", "Data Retrieved from axios request");
    // route to new page by changing window.location
    window.open('http://localhost:3000/medicover', "_blank");

  }

}

export default UnitatiMedicale;
