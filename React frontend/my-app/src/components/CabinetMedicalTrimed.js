import ZaharieFlorin from '../images/zaharie_florin.png';
import NeculaAlexandru from '../images/necula_alexandru.png';
import IancuCornel from '../images/iancu-cornel.jpg';
import StancaMarius from '../images/stanca_marius.png';

import React, { Component } from 'react';
import Image from 'react-bootstrap/Image';
import axios from 'axios';

const Doctor = (props) => (
  <div>
    <div class="container">
      <Image src={props.profilePicture} className="scale-with-grid" alt="Avatar" class="image" />
      <div class="overlay">
        <div class="text">{props.specialty}</div>
      </div>
    </div>
    <h2>{props.fullName}</h2>
    <p>{props.graduationYear}</p>
  </div>
);
const Doctor1 = (props) => (
  <div class="overlay">
    <div class="text">{props.specialty}</div>
  </div>
);

const Doctor2 = (props) => (
  <div>
    <h2>{props.fullName}</h2>
    <h4>Data absolvirii: </h4><p>{props.graduationYear}</p>

  </div>
);

class CabinetMedicalTrimed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctors: [] //in stare am acest array
    };
  }

  //toate rest callurile se fac aici
  async componentDidMount() {
    const result = await axios.get("http://localhost/cosmina/trimed.php");
    this.setState({
      doctors: result.data //populam state-ul cu ~ v[i] elem curent, din backend
    });
  }
    render() {
      return (
        <div>
        <div className="medsan-container">
          <h2>Medsan</h2>
          <div class="row">
            <div class="column" >
              <div class="container">
                <Image src={ZaharieFlorin} className="scale-with-grid" alt="Avatar" class="image" />
                {this.state.doctors.filter(i => i.doctorID == 19).map(i => <Doctor1 {...i} />)}
              </div>
              {this.state.doctors.filter(i => i.doctorID == 19).map(i => <Doctor2 {...i} />)}
            </div>

            <div class="column" >
              <div class="container">
                <Image src={IancuCornel} className="scale-with-grid" alt="Avatar" class="image" />
                {this.state.doctors.filter(i => i.doctorID == 20).map(i => <Doctor1 {...i} />)}
              </div>
              {this.state.doctors.filter(i => i.doctorID == 20).map(i => <Doctor2 {...i} />)}
            </div>

            <div class="column" >
              <div class="container">
                <Image src={StancaMarius} className="scale-with-grid" alt="Avatar" class="image" />
                {this.state.doctors.filter(i => i.doctorID == 21).map(i => <Doctor1 {...i} />)}
              </div>
              {this.state.doctors.filter(i => i.doctorID == 21).map(i => <Doctor2 {...i} />)}

            </div>
          </div>
        </div>
      </div>
      //   <div>
      //   <div className="medsan-container">
      //     <h2>Trimed</h2>
      //     <div class="row">
      //       <div class="column" >
      //         {this.state.doctors.map(i => <Doctor {...i} />)} 
      //         {/*pt fiecare element din state, vom randa un nou doctor cu propr coresp din state */}
      //       </div>
      //     </div>
      //   </div>
      // </div>
      ////////////////
      //   <div className="medsan-container">
      //   <h2>Cabinet Medical Trimed</h2>
      //   <div class="row">
      //     <div class="column" >
      //       <div class="container">
      //         <Image src={ZaharieFlorin} className="scale-with-grid" alt="Avatar" class="image" />
      //         <div class="overlay">
      //           <div class="text">medic psiholog</div>
      //         </div>
      //       </div>
      //       DR. ZAHARIE FLORIN
      //     <p>An absolvire: 2012</p>
      //     </div>
      //     <div class="column" >
      //       <div class="container">
      //         <Image src={NeculaAlexandru} className="scale-with-grid" alt="Avatar" class="image" />
      //         <div class="overlay">
      //           <div class="text">medic homeopat</div>
      //         </div>
      //       </div>
      //       DR.   BALDEAN ROBERT
      //     <p>An absolvire: 2017</p>
      //     </div>
      //     <div class="column" ><div class="container">
      //       <Image src={IancuCornel} className="scale-with-grid" alt="Avatar" class="image" />
      //       <div class="overlay">
      //         <div class="text">medic de familie</div>
      //       </div>
      //     </div>
      //       DR. BARTOS ADRIAN
      //     <p>An absolvire: 2016</p>
      //     </div>
      //   </div>
      // </div>
      )
    }
  }

  export default CabinetMedicalTrimed;