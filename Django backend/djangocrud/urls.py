from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    # path('', include('frontend.urls')),
    path('', include('djangocrud.api.urls')),
    path('admin/', admin.site.urls),
    # path('', include('accounts.urls'))
]
#
# from django.contrib import admin
# from django.urls import include, path
# from rest_framework import routers
# from djangocrud.api import views
#
# router = routers.DefaultRouter()
# router.register(r'movies', views.MovieViewSet)  # include viewSet to url
#
# # Wire up our API using automatic URL routing.
# # Additionally, we include login URLs for the browsable API.
# urlpatterns = [
#     path('admin/', admin.site.urls),
#     path('', include(router.urls)),  # include viewSet to url
#     path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
# ]
