import React, { Component } from 'react';
import '../App.css';
import t from '../translate/Locale';
import Fundal3 from '../images/homeImage.jpg';
import Fundal2 from '../images/business.jpg';
import Fundal from '../images/arms.jpg';
import Carousel from 'react-bootstrap/Carousel';

class HomeBody extends Component {
    render() {
        return <div>

            <Carousel>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={Fundal}
                        alt="First slide"
                    />
                    <Carousel.Caption>
                        <h1 className="fit-title">{t('NumeSite')}</h1>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={Fundal2}
                        alt="Third slide"
                    />
                    <Carousel.Caption>
                        <h1 className="fit-quote-2">{t('Slide2')}</h1>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={Fundal3}
                        alt="Third slide"
                    />
                    <Carousel.Caption>
                        <h1 className="fit-quote-3">{t('Slide3')}</h1>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>

        </div>
    }
}

export default HomeBody;