import React, { Component } from 'react';
import '../App.css';
import t from '../translate/Locale';
import Fundal3 from '../images/homeImage.jpg';
import Fundal2 from '../images/business.jpg';
import Fundal from '../images/arms.jpg';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import Carousel from 'react-bootstrap/Carousel';
import { UncontrolledAlert } from 'reactstrap';
import HomeBody from './HomeBody';
class Home extends Component {

  render() {
      if (sessionStorage.getItem("userData")) {
        return(
          <div>
            <UncontrolledAlert color="info">
              You are logged in!
            </UncontrolledAlert>
            <HomeBody />
          </div>)
      }
      else {
        return(
        <div>
          <UncontrolledAlert color="danger">
            You are not logged in!
          </UncontrolledAlert>
          <HomeBody />
        </div>)

      }

  }
}

export default Home;
