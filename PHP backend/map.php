<?php
include 'functions.php';

if (enable_cors()) {
    return;
}

$requestBody = json_decode(file_get_contents('php://input'));

$con = connect_to_db();

$fkUserId = $requestBody->fkUserId;
$hospitals=[];
$query=
"select hospitalName, latitude, longitude
from userlocationhospitals
where fkUserId='$fkUserId';";

if ($result = mysqli_query($con, $query)) {
    // Fetch one and one row
    while($row=mysqli_fetch_object($result))
		{
			$hospitals[]=$row;		
		}
		//Free result set
		mysqli_free_result($result);
}

mysqli_close($con);

echo json_encode($hospitals);
