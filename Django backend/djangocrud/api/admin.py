from django.contrib import admin
from .models import Doctors, Doctorspermedicalunit, User, Userdetails, Medicalunits,Usertype,Specialty,Unittype

admin.site.register(Doctors)
admin.site.register(Medicalunits)
admin.site.register(User)
admin.site.register(Userdetails)
admin.site.register(Doctorspermedicalunit)
admin.site.register(Usertype)
admin.site.register(Unittype)
admin.site.register(Specialty)