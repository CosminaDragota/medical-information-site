from rest_framework import routers
from .api import DoctorViewSet,UserViewSet,HospitalViewSet

router = routers.DefaultRouter()
router.register('api/doctors', DoctorViewSet, 'doctors')
router.register('api/users', UserViewSet, 'users')
router.register('api/hospitals', HospitalViewSet, 'hospitals')
urlpatterns = router.urls