from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Specialty,Medicalunits,User,Doctors, Unittype,Usertype


class DoctorSerializer(serializers.ModelSerializer):
    specialty = serializers.ChoiceField(choices=Specialty.objects.values_list('specialtyid', 'description'))

    class Meta:
        model = Doctors
        fields = '__all__'

class MedicalUnitSerializer(serializers.ModelSerializer):
    unittype = serializers.ChoiceField(choices=Unittype.objects.values_list('unittypeid', 'description'))

    class Meta:
        model = Medicalunits
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    usertype = serializers.ChoiceField(choices=Usertype.objects.values_list('usertypeid', 'description'))

    class Meta:
        model = User
        fields = '__all__'