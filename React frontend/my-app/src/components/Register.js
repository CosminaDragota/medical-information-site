import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { UncontrolledAlert } from 'reactstrap';


const imgMyimageexample = require('../images/background/doctor1.jpg');
const divStyle = {
    margin: '0',
    height: '100%',
    backgroundImage: `url(${imgMyimageexample})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat'
};

class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            password: "",
            userType: "simpleUser",
            nameValid: false,
            emailValid: false,
            passwordValid: false,
            submitDisable: true,
            registerUnsuccessful: "0" //stare initiala in care nu am apasat pe buton

        };
    }

    handleNameChange = event => {
        let nameValid = event.target.value ? true : false;
        let submitValid = this.state.passwordValid && nameValid;
        this.setState({
            nameValid: nameValid,
            submitDisable: !submitValid,
            name: event.target.value
        });
    }
    handleEmailChange = event => {
        let emailValid = event.target.value ? true : false;
        let submitValid = this.state.passwordValid && emailValid;
        // event.target returns the element that triggered the event

        this.setState({
            emailValid: emailValid,
            submitDisable: !submitValid,
            email: event.target.value
        });
        console.log(this.state);

    };

    handlePasswordChange = event => {
        let passwordValid = event.target.value ? true : false;
        let submitValid = this.state.emailValid && passwordValid;
        // event.target returns the element that triggered the event

        this.setState({
            passwordValid: passwordValid,
            submitDisable: !submitValid,
            password: event.target.value
        });
        console.log(this.state);

    };

    handleSubmit = event => {
        //exista un event default care poate declansa onSubmit, iar not nu vrem asta
        event.preventDefault();
        const user = axios
            .post("http://localhost/cosmina/register.php", this.state) //trimit in backend datele
            .then(
                 (response)=> {
                    console.log("date din backend", response.data);//vei primi rezultatele de la backend daca acestea au fost trimise
                    console.log("rolul", response.data.userType);
                    if (response.data) {
                        this.setState({
                            registerUnsuccessful: "1"
                        });
                    }
                    else {
                        console.log("Register failed!");

                    }
                }
            )
            .catch(function (error) {
                console.log(error);
            });
        console.log(user);
    };

    render() {
        if (this.state.registerUnsuccessful === "2") { //fara success
            return (
                <div>
                    <UncontrolledAlert color="danger">
                        Register Failed!
                    </UncontrolledAlert>
                    <div className="page-container" style={divStyle}>
                        <Form onSubmit={this.handleSubmit} className="center-form">
                            <Form.Row >
                                <Form.Group as={Col} md="3" controlId="formBasicPassword" className='add-margin-to-form'>
                                    <Form.Label>Full Name</Form.Label>
                                    <Form.Control
                                        name="name"
                                        type="name"
                                        placeholder="Your name here"
                                        onChange={this.handleNameChange} />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} md="3" controlId="formBasicEmail">
                                    <Form.Label>Email address</Form.Label>
                                    <Form.Control
                                        name="email"
                                        type="email"
                                        placeholder="Your email here"
                                        onChange={this.handleEmailChange} />
                                    <Form.Text className="text-muted">
                                        We'll never share your email with anyone else.
                                    </Form.Text>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} md="3" controlId="formBasicPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control
                                        name="password"
                                        type="password"
                                        placeholder="Your password here"
                                        onChange={this.handlePasswordChange} />
                                </Form.Group>
                            </Form.Row>
                            <br />
                            <Form.Row>
                                <Form.Group as={Col} md="3">
                                    <Form.Label className="label-size-terms-conditions">By checking the checkbox below you agree to out Terms that you have read our Data Use Policy and you agree to store your data into Session.</Form.Label>
                                    <Form.Check
                                        required
                                        name="terms"
                                        label="Agree to terms and conditions"
                                    />
                                </Form.Group>
                            </Form.Row>

                            <Button variant="primary" type="submit" disabled={this.state.submitDisable} className="register-button">
                                Register
                             </Button>
                        </Form>
                    </div>
                </div>)
        }
        if (this.state.registerUnsuccessful === "1") { // success
            return (
                <div>
                    <UncontrolledAlert color="success">
                        Register Successful!
                    </UncontrolledAlert>
                    <div className="page-container" style={divStyle}>
                        <Form onSubmit={this.handleSubmit} className="center-form">
                            <Form.Row >
                                <Form.Group as={Col} md="3" controlId="formBasicPassword" className='add-margin-to-form'>
                                    <Form.Label>Full Name</Form.Label>
                                    <Form.Control
                                        name="name"
                                        type="name"
                                        placeholder="Your name here"
                                        onChange={this.handleNameChange} />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} md="3" controlId="formBasicEmail">
                                    <Form.Label>Email address</Form.Label>
                                    <Form.Control
                                        name="email"
                                        type="email"
                                        placeholder="Your email here"
                                        onChange={this.handleEmailChange} />
                                    <Form.Text className="text-muted">
                                        We'll never share your email with anyone else.
                                    </Form.Text>
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} md="3" controlId="formBasicPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control
                                        name="password"
                                        type="password"
                                        placeholder="Your password here"
                                        onChange={this.handlePasswordChange} />
                                </Form.Group>
                            </Form.Row>
                            <br />
                            <Form.Row>
                                <Form.Group as={Col} md="3">
                                    <Form.Label className="label-size-terms-conditions">By checking the checkbox below you agree to out Terms that you have read our Data Use Policy and you agree to store your data into Session.</Form.Label>
                                    <Form.Check
                                        required
                                        name="terms"
                                        label="Agree to terms and conditions"
                                    />
                                </Form.Group>
                            </Form.Row>

                            <Button variant="primary" type="submit" disabled={this.state.submitDisable} className="register-button">
                                Register
                             </Button>
                        </Form>
                    </div>
                </div>)
        }
        if (this.state.registerUnsuccessful === "0") {//nu am apasat pe buton
        return (
            <div className="page-container" style={divStyle}>
                <Form onSubmit={this.handleSubmit} className="center-form">
                    <Form.Row >
                        <Form.Group as={Col} md="3" controlId="formBasicPassword" className='add-margin-to-form'>
                            <Form.Label>Full Name</Form.Label>
                            <Form.Control
                                name="name"
                                type="name"
                                placeholder="Your name here"
                                onChange={this.handleNameChange} />
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col} md="3" controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                name="email"
                                type="email"
                                placeholder="Your email here"
                                onChange={this.handleEmailChange} />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                </Form.Text>
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col} md="3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                name="password"
                                type="password"
                                placeholder="Your password here"
                                onChange={this.handlePasswordChange} />
                        </Form.Group>
                    </Form.Row>
                    <br />
                    <Form.Row>
                        <Form.Group as={Col} md="3">
                            <Form.Label className="label-size-terms-conditions">By checking the checkbox below you agree to out Terms that you have read our Data Use Policy and you agree to store your data into Session.</Form.Label>
                            <Form.Check
                                required
                                name="terms"
                                label="Agree to terms and conditions"
                            />
                        </Form.Group>
                    </Form.Row>

                    <Button variant="primary" type="submit" disabled={this.state.submitDisable} className="register-button">
                        Register
                </Button>
                </Form>
            </div>

        )}
    }

}

export default Register;