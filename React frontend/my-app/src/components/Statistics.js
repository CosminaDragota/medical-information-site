import React, { Component } from 'react';
import Chart from './Chart';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

const medii=[];

class Statistics extends Component{
    constructor(){
        super();
        this.state={
            chartData:{},
            note:[]
        }
    }

    componentWillMount(){
        this.getChartData();      
        this.renderChartData();
    }


    renderChartData(){
        this.setState({
            chartData:{
                labels:[
                    'Medsan', 
                    'Policlinica Medstar', 
                    'Centrul Medical Asteco',
                    'New Medica',
                    'Hyperclinica Medlife',
                    'Blue Life Medical Center',
                    'Cabinet Medical Trimed',
                    'Policlinica Promedis',
                    'Medicover Republicii'
                ],
                datasets:[
                    {
                        label: 'Rating',
                        data:[
                            sessionStorage.getItem("1"),
                            sessionStorage.getItem("2"),
                            sessionStorage.getItem("3"),
                            sessionStorage.getItem("4"),
                            sessionStorage.getItem("5"),
                            sessionStorage.getItem("6"),
                            sessionStorage.getItem("7"),
                            sessionStorage.getItem("8"),
                            sessionStorage.getItem("9"),
                                                                                 
                        ],
                        backgroundColor:[
                           'rgba( 255  , 99  , 132  , 0.6)',
                           'rgba( 54  , 162  , 235  , 0.6)',
                           'rgba( 255  ,  206 , 86  , 0.6)',
                           'rgba( 75  , 192  , 192  , 0.6)',
                           'rgba( 153  , 102  , 255  , 0.6)',
                           'rgba( 255  , 159  ,  64 , 0.6)',
                           'rgba( 255  , 99  , 132  , 0.6)',
                           'rgba( 54  , 162  , 235  , 0.6)',
                           'rgba( 255  ,  206 , 86  , 0.6)',
                           'rgba( 75  , 192  , 192  , 0.6)'
                        ]
                    }
                ]
            }
        })
    }
      async getChartData(){
        //Ajax Call
        //fill state with data that comes in
        let user = sessionStorage.getItem('userData');
        let objectUser = JSON.parse(user);
        if (user) {
        const result = await axios.get("http://localhost/cosmina/grafic.php");
        this.setState({
          note: result.data //populam state-ul cu ~ v[i] elem curent, din backend
        });
        console.log("This is result data ", result.data);
        console.log("This is chart data ", this.state.note);
        console.log("Medii ", 
                           this.state.note[0].media,
                            this.state.note[1].media,
                            this.state.note[2].media,
                            this.state.note[3].media,
                            this.state.note[4].media,
                            this.state.note[5].media,
                            this.state.note[6].media,
                            this.state.note[7].media,
                            this.state.note[8].media );
        sessionStorage.setItem("1", this.state.note[0].media);
        sessionStorage.setItem("2", this.state.note[1].media);
        sessionStorage.setItem("3", this.state.note[2].media);
        sessionStorage.setItem("4", this.state.note[3].media);
        sessionStorage.setItem("5", this.state.note[4].media);
        sessionStorage.setItem("6", this.state.note[5].media);
        sessionStorage.setItem("7", this.state.note[6].media);
        sessionStorage.setItem("8", this.state.note[7].media);
        sessionStorage.setItem("9", this.state.note[8].media);
    }

        
    }

    render(){
        let user = sessionStorage.getItem('userData');
        let objectUser = JSON.parse(user);
        if (user) {
        return(
            <div>              
                <Chart chartData={this.state.chartData} legendPosition="bottom"/> 
            </div>
        )}
        else {
            return <Redirect to='*' />;
        }
    }
}
export default Statistics;