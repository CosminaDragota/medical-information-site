import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Map, InfoWindow, Marker, GoogleApiWrapper, Listing } from 'google-maps-react';
import axios from 'axios';

const style = {
    width: '100%',
    height: '100%'
}

class MyMap extends Component {
    constructor() {
        super();
        this.state = ({
            fkUserId: ''
        })
    }

    render() {
        let user = sessionStorage.getItem('userData');
        let objectUser = JSON.parse(user);
        if (user) {
            var hospitalsString = sessionStorage.getItem('hospitals');
            let objectHospitals = JSON.parse(hospitalsString);
            var myHospitals = {
                hospitals: objectHospitals
            }
            if (hospitalsString) {
                console.log(myHospitals);
            }

            return (
                <div>
                    <Map
                        google={this.props.google}
                        style={style}
                        initialCenter={{
                            lat: objectUser.latitude,
                            lng: objectUser.longitude
                        }}
                        zoom={13}

                        visible={true}
                    >
                        <Marker
                            title={'Locatia ta'}
                            position={{ lat: objectUser.latitude, lng: objectUser.longitude }} />
                        <Marker
                            title={myHospitals.hospitals[0].hospitalName}
                            position={{ lat: myHospitals.hospitals[0].latitude, lng: myHospitals.hospitals[0].longitude }} />
                        <Marker
                            title={myHospitals.hospitals[1].hospitalName}
                            position={{ lat: myHospitals.hospitals[1].latitude, lng: myHospitals.hospitals[1].longitude }} />
                        <Marker
                            title={myHospitals.hospitals[2].hospitalName}
                            position={{ lat: myHospitals.hospitals[2].latitude, lng: myHospitals.hospitals[2].longitude }} />

                        <InfoWindow onClose={this.onInfoWindowClose}>
                        </InfoWindow>
                    </Map>
                </div>
            );
        } else {
            return <Redirect to='*' />;
        }

    }
}

//export default MyMap;
export default GoogleApiWrapper({
    apiKey: ("AIzaSyAXTA4mvFDkotbH2j-JrGnOXWcHro4guLE")
})(MyMap)