import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import { Redirect } from 'react-router-dom';
import { UncontrolledAlert } from 'reactstrap';
import Form from 'react-bootstrap/Form';


const imgMyimageexample = require('../images/background/doctor1.jpg');
const divStyle = {
    margin: '0',
    height: '100%',
    backgroundImage: `url(${imgMyimageexample})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat'
};
class AdminManager extends Component {
    constructor(){
        super();
        this.state={
            redirectTo:''
        }
    }

    handleSubmitClinics = (event) => {
        //exista un event default care poate declansa onSubmit, iar not nu vrem asta
        event.preventDefault();
        this.setState({
            redirectTo:"clinics"
        })

    }

    handleSubmitDoctors = (event) => {
        //exista un event default care poate declansa onSubmit, iar not nu vrem asta
        event.preventDefault();
        this.setState({
            redirectTo:"doctors"
        })
        console.log("submit doctors");
    }

    handleSubmitUsers = (event) => {
        //exista un event default care poate declansa onSubmit, iar not nu vrem asta
        event.preventDefault();
        this.setState({
            redirectTo:"users"
        })

    }

    render() {
        let user = sessionStorage.getItem('userData');
        let objectUser = JSON.parse(user);
        if (user) {

            if (objectUser.userType === "admin") {

                return (
                <div className="page-container-links" style={divStyle}>
                    <UncontrolledAlert color="success">
                        Welcome admin!
                </UncontrolledAlert>
                <div className="page-container-links">
                <div className="links-margin">
                <br /><br />
                        <a href="http://localhost:3000/clinicscrud">Modify Clinics...</a>
                <br />

                <br />
                        <a href="http://localhost:3000/doctorscrud">Modify Doctors...</a>
                <br />

                <br />
                        <a href="http://localhost:3000/userscrud">Delete Accounts...</a>
                <br /><br />
                </div>
                </div>
                </div>)
            }
            if (objectUser.userType === "simpleUser") {
                return <Redirect to='/user' />;
            }
            
        } else {
            return <Redirect to='*' />;
        }
        

    }

}

export default AdminManager;