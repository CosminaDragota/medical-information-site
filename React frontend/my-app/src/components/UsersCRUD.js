import React, { Component } from 'react';
import { getList, addItem, deleteItem, updateItem } from './UsersCRUDFunctions';
import { Redirect } from 'react-router-dom';

const imgMyimageexample = require('../images/background/doctor1.jpg');
const divStyle = {
    margin: '0',
    height: '100%',
    backgroundImage: `url(${imgMyimageexample})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat'
};

const tableColor={
    backgroundColor: 'burlywood'
};

class UsersCRUD extends Component {
    constructor() {
        super()
        this.state = {
            userid: '',
            usertype: '',
            email: '',
            password: '',
            editDisabled: false,
            items: []
        }
    }

    componentDidMount() {
        this.getAll()
    }


    getAll = () => {
        getList().then(data => {
            this.setState({
                userid: '',
                usertype: '',
                email: '',
                password: '',
                items: [...data]
            },
                () => {
                    console.log(this.state.items)
                })
        })
    }

    onEdit = (itemid, e) => {
        e.preventDefault();

        sessionStorage.setItem('clinic', JSON.stringify(this.state));
        console.log("On edit", itemid);

        var data = [...this.state.items]
        data.forEach((item, index) => {
            if (item.userid === itemid) {
                console.log("true");
                this.setState({
                    userid: item.userid,
                    usertype: item.usertype,
                    email: item.email,
                    password: item.password,
                    editDisabled: true
                })
            }
        })
    }

    onDelete = (val, e) => {
        e.preventDefault();
        deleteItem(val);
        this.getAll();
    }

    render() {
        let user = sessionStorage.getItem('userData');
        let objectUser = JSON.parse(user);
        if (user) {

            if (objectUser.userType === "admin") {

                return (
                    <div className="page-container-links" style={divStyle}>
                        <div className="col-md-4">
                            <h2>Existing Accounts:</h2>
                            <br/>
                            <div className="table-color">
                            <table className="table">
                                <tr>
                                    <th>Role</th>
                                    <th>Email</th>
                                    <th></th>
                                </tr>
                                <tbody>
                                    {this.state.items.map((item, index) => (
                                        <tr key={index}>
                                            <td className="text-left">{item.usertype} </td>
                                            <td className="text-left">{item.email} </td>
                                            <td className="text-right">

                                                <button
                                                    href=""
                                                    className="btn btn-danger"
                                                    disabled={this.state.editDisabled}
                                                    onClick={this.onDelete.bind(
                                                        this,
                                                        item.userid
                                                    )}
                                                >
                                                    Delete
                                    </button>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                            </div>

                        </div>
                    </div>
                )
            }
            if (objectUser.userType === "simpleUser") {
                return <Redirect to='/user' />;
            }

        } else {
            return <Redirect to='*' />;
        }
    }
}

export default UsersCRUD;