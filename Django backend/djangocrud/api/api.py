from .models import Doctors
from rest_framework import viewsets, permissions
from .serializers import DoctorSerializer,UserSerializer,MedicalUnitSerializer
from .models import User,Doctors,Medicalunits

class DoctorViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.AllowAny,  # change later to: IsAuthenticated
    ]
    serializer_class = DoctorSerializer
    queryset = Doctors.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(self, request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(self, request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(self, request, *args, **kwargs)

class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.AllowAny,  # change later to: IsAuthenticated
    ]
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(self, request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(self, request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(self, request, *args, **kwargs)

class HospitalViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.AllowAny,  # change later to: IsAuthenticated
    ]
    serializer_class = MedicalUnitSerializer
    queryset = Medicalunits.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(self, request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(self, request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(self, request, *args, **kwargs)