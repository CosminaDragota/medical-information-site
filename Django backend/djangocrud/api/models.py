from django.db import models

class Doctors(models.Model):
    doctorid = models.AutoField(db_column='doctorID', primary_key=True)  # Field name made lowercase.
    fullname = models.CharField(db_column='fullName', max_length=255)  # Field name made lowercase.
    graduationyear = models.DateField(db_column='graduationYear', blank=True, null=True)  # Field name made lowercase.
    specialty = models.CharField(max_length=19)
    profilepicture = models.ImageField(db_column='profilePicture', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'doctors'

class Medicalunits(models.Model):
    medicalunitid = models.AutoField(db_column='medicalUnitID', primary_key=True)
    name = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    unittype = models.CharField(max_length=16)  # Field name made lowercase.
    logo = models.CharField(db_column='logo', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'medicalunits'

class Doctorspermedicalunit(models.Model):
    doctorpermedicalunitid = models.AutoField(db_column='doctorPerMedicalUnitID', primary_key=True)
    fkmedicalunits =models.IntegerField()  #models.ForeignKey(Medicalunits, on_delete=models.CASCADE) # Field name made lowercase.
    fkdoctors = models.IntegerField() #models.ForeignKey(Doctors, on_delete=models.CASCADE)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'doctorspermedicalunit'

class Unittype(models.Model):
    unittypeid = models.AutoField(db_column='unittypeID', primary_key=True)  # Field name made lowercas
    description = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'unittype'

class Specialty(models.Model):
    specialtyid = models.AutoField(db_column='specialtyID', primary_key=True)  # Field name made lowercas
    description = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'specialty'

class Usertype(models.Model):
    usertypeid = models.AutoField(db_column='usertypeID', primary_key=True)  # Field name made lowercas
    description = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'usertype'


class User(models.Model):
    userid = models.AutoField(db_column='userID', primary_key=True)  # Field name made lowercase.
    email = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    usertype = models.CharField(db_column='userType', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'user'


class Userdetails(models.Model):
    userdetailsid = models.AutoField(db_column='userDetailsID', primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    disease = models.CharField(max_length=18, blank=True, null=True)
    profileimage = models.ImageField(db_column='profileImage', blank=True, null=True)
    fkuserid = models.IntegerField()# models.ForeignKey(User, on_delete=models.CASCADE)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'userdetails'
